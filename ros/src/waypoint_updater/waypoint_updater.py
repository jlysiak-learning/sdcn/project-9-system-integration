#!/usr/bin/env python

import numpy as np
import rospy
from std_msgs.msg import Int32
from geometry_msgs.msg import PoseStamped
from styx_msgs.msg import Lane, Waypoint

import math
from scipy.spatial import KDTree



class WaypointUpdater(object):

    # Number of waypoints we will publish.
    LOOKAHEAD_WPS = 150

    # Main loop rate in Hz
    RATE = 25

    def __init__(self):
        rospy.init_node('waypoint_updater', log_level=rospy.DEBUG)

        rospy.Subscriber('/current_pose', PoseStamped, self.pose_cb)
        rospy.Subscriber('/base_waypoints', Lane, self.waypoints_cb)
        rospy.Subscriber('/traffic_waypoint', Int32, self.traffic_cb)

        self.final_waypoints_pub = rospy.Publisher('final_waypoints', Lane, queue_size=1)

        self.pose = None
        self.base_waypoints = None
        self.waypoints_2d = None
        self.waypoints_tree = None
        self.traffic_waypoint = None

        rospy.loginfo("Node initialized! Launching...")
        self.main()

    def main(self):
        rate = rospy.Rate(self.RATE)
        while not rospy.is_shutdown():
            if self.pose is not None and self.base_waypoints is not None:
                self.publish_final_waypoints()
            rate.sleep()

    def pose_cb(self, msg):
        self.pose = msg

    def waypoints_cb(self, waypoints):
        rospy.logdebug("Base waypoints received:\n%s\n", waypoints)
        if not self.waypoints_2d:
            self.waypoints_2d = [ [wp.pose.pose.position.x,
                                   wp.pose.pose.position.y] for wp in
                                        waypoints.waypoints ]
            self.waypoints_tree = KDTree(self.waypoints_2d)
        # Set base_waypoints after all init is done, as all checks are done
        # on this object
        self.base_waypoints = waypoints

    def traffic_cb(self, msg):
        self.traffic_waypoint = msg # Nearest red light idx

    def obstacle_cb(self, msg):
        # TODO: Callback for /obstacle_waypoint message. We will implement it later
        pass

    def get_xy(self):
        return [self.pose.pose.position.x, self.pose.pose.position.y]

    def get_consequtive_waypoints(self, idx):
        return (np.array(self.waypoints_2d[idx]), idx), \
               (np.array(self.waypoints_2d[idx+1]),
                       (idx + 1) % len(self.waypoints_2d))

    def publish_final_waypoints(self):
        idx, wp = self.find_nearest_waypoint()
        start = idx
        stop = start + self.LOOKAHEAD_WPS
        waypoints = self.base_waypoints.waypoints[start:stop]

        if self.traffic_waypoint is not None:
            traffic_idx = self.traffic_waypoint.data
            if traffic_idx > start + 1 and traffic_idx < stop:
                waypoints = self.slow_down(waypoints, traffic_idx - start)
        msg = Lane()
        msg.header = self.base_waypoints.header
        msg.waypoints = waypoints
        self.final_waypoints_pub.publish(msg)

    def slow_down(self, waypoints, stop_idx):
        a_max = .5 # Deceleration limit, m/s^2
        dist = lambda wp, wp_: math.sqrt(
                (wp.pose.pose.position.x - wp_.pose.pose.position.x) ** 2 + \
                (wp.pose.pose.position.y - wp_.pose.pose.position.y) ** 2)
        def v(dx, v_next):
            v2 = v_next*v_next - 2 * a_max * dx
            if v2 < 0:
                return 0
            return math.sqrt(v2)

        new_waypoints = [ Waypoint() for _ in waypoints[:stop_idx+1] ]
        for i in range(stop_idx+1):
            new_waypoints[i].pose = waypoints[i].pose
            new_waypoints[i].twist.twist.linear.x = 0

        stop_idx = stop_idx - 5
        # Stop the car at stop_idx
        for i in range(stop_idx, -1, -1): # go from stop_idx to 0 (inclusive)
            s = dist(new_waypoints[i], new_waypoints[stop_idx])
            # Compute velocity at the current waypoint
            v_curr = math.sqrt(2 * s * a_max)
            if v_curr > waypoints[i].twist.twist.linear.x:
                v_curr = waypoints[i].twist.twist.linear.x
            new_waypoints[i].twist.twist.linear.x = v_curr
        return new_waypoints


    def find_nearest_waypoint(self):
        '''Given pose and base_waypoints returns the nearest waypoint'''
        assert self.pose
        assert self.base_waypoints
        pos = self.get_xy()
        neighbors_n = 1
        _, idx = self.waypoints_tree.query(pos, k=neighbors_n)
        car = np.array(pos)
        (w1, i1), (w2, i2) = self.get_consequtive_waypoints(idx)
        # Get the point ahead of car

        # Vector pointing from car to the closest waypoint
        v1 = w1 - car
        # Vector showing the correct waypoint direction
        v2 = w2 - w1
        if np.dot(v1, v2) > 0:
            # Positive dot product implies that the first point is already
            # ahead of the car
            return i1, w1
        # Negative dot product, direction from car to the closest waypoint is
        # actually opposite to the waypoints direction
        return i2, w2

    def get_waypoint_velocity(self, waypoint):
        return waypoint.twist.twist.linear.x

    def set_waypoint_velocity(self, waypoints, waypoint, velocity):
        waypoints[waypoint].twist.twist.linear.x = velocity

    def car_waypoint_distance(self, car_waypoint, dst_waypoint):
        dl = lambda a, b: math.sqrt((a.x-b.x)**2 + (a.y-b.y)**2  + (a.z-b.z)**2)
        return dl(car_waypoint.pose.pose.position, dst_waypoint.pose.pose.position)

    def distance(self, waypoints, wp1, wp2):
        dist = 0
        dl = lambda a, b: math.sqrt((a.x-b.x)**2 + (a.y-b.y)**2  + (a.z-b.z)**2)
        for i in range(wp1, wp2+1):
            dist += dl(waypoints[wp1].pose.pose.position, waypoints[i].pose.pose.position)
            wp1 = i
        return dist


if __name__ == '__main__':
    try:
        WaypointUpdater()
    except rospy.ROSInterruptException:
        rospy.logerr('Could not start waypoint updater node.')

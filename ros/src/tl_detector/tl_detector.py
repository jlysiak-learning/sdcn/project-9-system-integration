#!/usr/bin/env python
import rospy
from std_msgs.msg import Int32
from geometry_msgs.msg import PoseStamped, Pose
from styx_msgs.msg import TrafficLightArray, TrafficLight
from styx_msgs.msg import Lane
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from light_classification.tl_classifier import TLClassifier
import tf
import cv2
import yaml
import numpy as np
from scipy.spatial import KDTree
import math

STATE_COUNT_THRESHOLD = 3

class TLDetector(object):
    def __init__(self):
        rospy.init_node('tl_detector')

        self.base_waypoints = None
        self.waypoints_2d = None
        self.waypoints_tree = None

        self.pose = None
        self.camera_image = None
        self.lights = []

        sub1 = rospy.Subscriber('/current_pose', PoseStamped, self.pose_cb)
        sub2 = rospy.Subscriber('/base_waypoints', Lane, self.waypoints_cb)

        '''
        /vehicle/traffic_lights provides you with the location of the traffic light in 3D map space and
        helps you acquire an accurate ground truth data source for the traffic light
        classifier by sending the current color state of all traffic lights in the
        simulator. When testing on the vehicle, the color state will not be available. You'll need to
        rely on the position of the light and the camera image to predict it.
        '''
        sub3 = rospy.Subscriber('/vehicle/traffic_lights', TrafficLightArray, self.traffic_cb)
        #sub6 = rospy.Subscriber('/image_color', Image, self.image_cb)

        config_string = rospy.get_param("/traffic_light_config")
        self.config = yaml.load(config_string)

        self.upcoming_red_light_pub = rospy.Publisher('/traffic_waypoint', Int32, queue_size=1)

        self.bridge = CvBridge()
        self.light_classifier = TLClassifier()
        self.listener = tf.TransformListener()

        self.state = TrafficLight.UNKNOWN
        self.last_state = TrafficLight.UNKNOWN
        self.last_wp = -1
        self.state_count = 0

        rospy.spin()

    def pose_cb(self, msg):
        self.pose = msg

    def waypoints_cb(self, waypoints):
        rospy.logdebug("Base waypoints received:\n%s\n", waypoints)
        if not self.waypoints_2d:
            self.waypoints_2d = [ [wp.pose.pose.position.x,
                                   wp.pose.pose.position.y] for wp in
                                        waypoints.waypoints ]
            self.waypoints_tree = KDTree(self.waypoints_2d)
        # Set base_waypoints after all init is done, as all checks are done
        # on this object
        self.base_waypoints = waypoints

    def traffic_cb(self, msg):
        '''Process lights using Ground Truth data.'''
        self.lights = msg.lights
        light_wp, state = self.process_traffic_lights()
        if self.state != state:
            self.state_count = 0
            self.state = state
        elif self.state_count >= STATE_COUNT_THRESHOLD:
            self.last_state = self.state
            light_wp = light_wp if state == TrafficLight.RED else -1
            self.last_wp = light_wp
            self.upcoming_red_light_pub.publish(Int32(light_wp))
        else:
            self.upcoming_red_light_pub.publish(Int32(self.last_wp))
        self.state_count += 1

    def image_cb(self, msg):
        """Identifies red lights in the incoming camera image and publishes the index
            of the waypoint closest to the red light's stop line to /traffic_waypoint

        Args:
            msg (Image): image from car-mounted camera

        """
        self.has_image = True
        self.camera_image = msg
        light_wp, state = self.process_traffic_lights()

        '''
        Publish upcoming red lights at camera frequency.
        Each predicted state has to occur `STATE_COUNT_THRESHOLD` number
        of times till we start using it. Otherwise the previous stable state is
        used.
        '''
        if self.state != state:
            self.state_count = 0
            self.state = state
        elif self.state_count >= STATE_COUNT_THRESHOLD:
            self.last_state = self.state
            light_wp = light_wp if state == TrafficLight.RED else -1
            self.last_wp = light_wp
            self.upcoming_red_light_pub.publish(Int32(light_wp))
        else:
            self.upcoming_red_light_pub.publish(Int32(self.last_wp))
        self.state_count += 1

    def process_traffic_lights(self):
        """Finds closest visible traffic light, if one exists, and determines its
            location and color

        Returns:
            int: index of waypoint closes to the upcoming stop line for a traffic light (-1 if none exists)
            int: ID of traffic light color (specified in styx_msgs/TrafficLight)

        """
        nearest_light_wp = None
        nearest_light_wp_xy = None
        if self.pose and self.base_waypoints is not None:
            #   List of positions that correspond to the line to stop
            # in front of for a given intersection
            stop_line_positions = self.config['stop_line_positions']
            car_wp_idx, car_wp_xy = self.get_closest_waypoint(
                    self.pose.pose.position.x,
                    self.pose.pose.position.y)
            min_idx_diff = len(self.base_waypoints.waypoints)
            for x, y in stop_line_positions:
                light_wp_idx, light_wp_xy = self.get_closest_waypoint(x, y)
                idx_diff = light_wp_idx - car_wp_idx
                if idx_diff < min_idx_diff and idx_diff >= 0:
                    nearest_light_wp = light_wp_idx
                    nearest_light_wp_xy = light_wp_xy
                    min_idx_diff = idx_diff

        if nearest_light_wp_xy is not None:
            state = self.get_light_state_from_gt(nearest_light_wp_xy)
            return nearest_light_wp, state
        return -1, TrafficLight.UNKNOWN

    def get_closest_waypoint(self, x, y):
        """Identifies the closest path waypoint to the given position
        Args:
            pose (Pose): position to match a waypoint to

        Returns:
            int: index of the closest waypoint in self.waypoints

        """
        assert self.base_waypoints
        pos = [x, y]
        neighbors_n = 1
        _, idx = self.waypoints_tree.query(pos, k=neighbors_n)
        car = np.array(pos)
        (w1, i1), (w2, i2) = self.get_consequtive_waypoints(idx)
        # Get the point ahead of car

        # Vector pointing from car to the closest waypoint
        v1 = w1 - car
        # Vector showing the correct waypoint direction
        v2 = w2 - w1
        if np.dot(v1, v2) > 0:
            # Positive dot product implies that the first point is already
            # ahead of the car
            return i1, w1
        # Negative dot product, direction from car to the closest waypoint is
        # actually opposite to the waypoints direction
        return i2, w2

    def get_consequtive_waypoints(self, idx):
        return (np.array(self.waypoints_2d[idx]), idx), \
               (np.array(self.waypoints_2d[idx+1]),
                       (idx + 1) % len(self.waypoints_2d))

    def get_light_state_from_gt(self, light_wp_xy):
        """Get light state from ground truth data."""
        light_state = TrafficLight.UNKNOWN
        if not self.lights:
            return light_state

        dist2 = lambda x1, y1, x2, y2: (x1 - x2) ** 2 + (y1 - y2) ** 2
        min_dist = float("inf")
        for light in self.lights:
            light_pos = light.pose.pose.position
            d = dist2(light_pos.x, light_pos.y, light_wp_xy[0], light_wp_xy[1])
            if min_dist > d:
                light_state = light.state
                min_dist = d
        return light_state

    def get_light_state(self, light):
        """Determines the current color of the traffic light

        Args:
            light (TrafficLight): light to classify

        Returns:
            int: ID of traffic light color (specified in styx_msgs/TrafficLight)

        """
        if(not self.has_image):
            self.prev_light_loc = None
            return False

        cv_image = self.bridge.imgmsg_to_cv2(self.camera_image, "bgr8")

        #Get classification
        return self.light_classifier.get_classification(cv_image)


if __name__ == '__main__':
    try:
        TLDetector()
    except rospy.ROSInterruptException:
        rospy.logerr('Could not start traffic node.')

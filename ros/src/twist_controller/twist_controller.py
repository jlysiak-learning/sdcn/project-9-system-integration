import rospy

from units import GAS_DENSITY, ONE_MPH
from params import PARAMS
from lowpass import LowPassFilter
from pid import PID
from yaw_controller import YawController


class Controller(object):
    def __init__(self, **params):
        self.check_params(params)
        self.params = params
        self.throttle_ctrl = PID(**params)
        self.yaw_ctrl = YawController(**params)
        self.velocity_filter = LowPassFilter(**params)
        self.last_time = rospy.get_time()

    def check_params(self, params):
        rospy.logerr(params)
        for k, _ in PARAMS:
            if k not in params:
                raise Exception("Parameter %s not defined!" % k)

    def control(self, enabled, linear_velocity, angular_velocity,
                current_velocity):
        throttle = 0
        brake = 0
        steering = 0
        if not enabled:
            # DBW disabled
            self.throttle_ctrl.reset()
            return throttle, brake, steering
        # Filter car velocity noise
        current_velocity = self.velocity_filter.update(current_velocity)
        steering = self.update_steering(linear_velocity, angular_velocity,
                current_velocity)
        throttle, brake = self.update_throttle_and_brake(linear_velocity,
                current_velocity)
        return throttle, brake, steering

    def update_steering(self, linear_velocity, angular_velocity,
                        current_velocity):
        return self.yaw_ctrl.get_steering(linear_velocity,
                    angular_velocity, current_velocity)

    def update_throttle_and_brake(self, linear_velocity, current_velocity):
        throttle = 0
        brake = 0

        self.throttle_ctrl.setpoint(linear_velocity)
        curr_time = rospy.get_time()
        dt = curr_time - self.last_time
        self.last_time = curr_time
        throttle = self.throttle_ctrl.update(current_velocity, dt)

        if linear_velocity < 0.1 and current_velocity < 0.2:
            throttle = 0
            brake = self.params['stop_car_brake_torque']
        elif throttle < 0.1 and current_velocity > linear_velocity:
            # Upps, we drive faster! Brake!
            throttle = 0
            # How fast we want to decelerate
            deceleration = (linear_velocity - current_velocity) / dt
            if deceleration < self.params['decel_limit']:
                # deceleration is negative! that's why we get max
                deceleration = self.params['decel_limit']
            # Compute braking torque: Force X Lever Arm
            brake = -deceleration * self.params['vehicle_mass'] * \
                    self.params['wheel_radius']
        return throttle, brake


#!/usr/bin/env python

import rospy
from std_msgs.msg import Bool
from dbw_mkz_msgs.msg import ThrottleCmd, SteeringCmd, BrakeCmd, SteeringReport
from geometry_msgs.msg import TwistStamped
import math

from twist_controller import Controller

from params import PARAMS

'''
You can build this node only after you have built (or partially built) the `waypoint_updater` node.

You will subscribe to `/twist_cmd` message which provides the proposed linear and angular velocities.
You can subscribe to any other message that you find important or refer to the document for list
of messages subscribed to by the reference implementation of this node.

One thing to keep in mind while building this node and the `twist_controller` class is the status
of `dbw_enabled`. While in the simulator, its enabled all the time, in the real car, that will
not be the case. This may cause your PID controller to accumulate error because the car could
temporarily be driven by a human instead of your controller.

We have provided two launch files with this node. Vehicle specific values (like vehicle_mass,
wheel_base) etc should not be altered in these files.

We have also provided some reference implementations for PID controller and other utility classes.
You are free to use them or build your own.

Once you have the proposed throttle, brake, and steer values, publish it on the various publishers
that we have created in the `__init__` function.

'''

class DBWNode(object):
    def __init__(self):
        self.enabled = False
        self.linear_velocity = None
        self.angular_velocity = None
        self.current_velocity = None

        rospy.init_node('dbw_node', log_level=rospy.DEBUG)
        self.steer_pub = rospy.Publisher('/vehicle/steering_cmd',
                                         SteeringCmd, queue_size=1)
        self.throttle_pub = rospy.Publisher('/vehicle/throttle_cmd',
                                            ThrottleCmd, queue_size=1)
        self.brake_pub = rospy.Publisher('/vehicle/brake_cmd',
                                         BrakeCmd, queue_size=1)
        params = self.load_params()
        self.controller = Controller(**params)

        rospy.Subscriber('/vehicle/dbw_enabled', Bool, self.cb_dbw_enabled)
        rospy.Subscriber('/current_velocity', TwistStamped, self.cb_current_velocity)
        rospy.Subscriber('/twist_cmd', TwistStamped, self.cb_twist_cmd)

        self.loop()

    def load_params(self):
        params = dict()
        for key, default in PARAMS:
            params[key] = rospy.get_param('~' + key, default)
        return params


    def cb_dbw_enabled(self, msg):
        '''State of DBW - if false, safety driver took control over the car.
            Autopilot has been disengaged. '''
        rospy.logdebug('DBW: %s', str(msg))
        self.enabled = msg

    def cb_current_velocity(self, msg):
        '''Twist cmd with a current velocity'''
        self.current_velocity = msg.twist.linear.x

    def cb_twist_cmd(self, msg):
        '''Twist cmd with velocity setpoints.'''
        self.linear_velocity = msg.twist.linear.x
        self.angular_velocity = msg.twist.angular.z

    def loop(self):
        # 50Hz is required by car
        rate = rospy.Rate(50)
        while not rospy.is_shutdown():
            if self.current_velocity is None or \
               self.linear_velocity is None or \
               self.angular_velocity is None:
                rospy.logdebug('Not all values not None! %s %s %s',
                        str(self.current_velocity), str(self.linear_velocity),
                        str(self.angular_velocity))
                rate.sleep()
                continue
            # Update controller
            throttle, brake, steering = self.controller.control(
                    self.enabled,
                    self.linear_velocity,
                    self.angular_velocity,
                    self.current_velocity)
            if self.enabled:
                rospy.logdebug("Th:%f Br:%f St:%f", throttle, brake, steering)
                self.publish(throttle, brake, steering)
            rate.sleep()

    def publish(self, throttle, brake, steer):
        tcmd = ThrottleCmd()
        tcmd.enable = True
        tcmd.pedal_cmd_type = ThrottleCmd.CMD_PERCENT
        tcmd.pedal_cmd = throttle
        self.throttle_pub.publish(tcmd)

        scmd = SteeringCmd()
        scmd.enable = True
        scmd.steering_wheel_angle_cmd = steer
        self.steer_pub.publish(scmd)

        bcmd = BrakeCmd()
        bcmd.enable = True
        bcmd.pedal_cmd_type = BrakeCmd.CMD_TORQUE
        bcmd.pedal_cmd = brake
        self.brake_pub.publish(bcmd)


if __name__ == '__main__':
    DBWNode()

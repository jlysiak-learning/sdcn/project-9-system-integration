# Params names and their default values.
# Custom values can be loaded from the parameter server
PARAMS = [
    ('vehicle_mass', 1736.35),
    ('fuel_capacity', 13.5),
    ('brake_deadband', .1),
    ('decel_limit', -5),
    ('accel_limit', 1),
    ('wheel_radius', 0.2413),
    ('stop_car_brake_torque', 400), # 400 Nm to keep car in place
    # Yaw controller params
    ('wheel_base', 2.8498),
    ('steer_ratio', 14.8),
    ('min_speed', 0.1),
    ('max_lat_accel', 3.),
    ('max_steer_angle', 8.),
    # Velocity PID controller
    ('kp', 0.3),
    ('ki', 0.1),
    ('kd', 0),
    ('mn', 0), # Min throttle value
    ('mx', 0.2), # Max throttle value
    # Velocity low-pass filter params
    ('tau', 0.5), # low-pass filter cutoff frequency = (2 * pi * tau)^-1
    ('ts', 0.02), # Sample time, 0.02 -> 50Hz
]

# Self-Driving Car Nanodegree final project: system integration

Project starter code can be found
[here](https://github.com/udacity/CarND-Capstone).

The project is a ROS-based system that controls a car is a simulator.

The simulator can be downloaded
[here](https://github.com/udacity/CarND-Capstone/releases).

And the original README is [here](./ORIG-README.md).

## Solution

Solution is mostly based on ideas from Q&A sessions.

`tl_detector` node publishes the nearest traffic light position based on
ground truth provided in the simulator.

## Running the code

1. Build a container. Run: `docker build . -t capstone`
2. Run the container: `./run-container.sh`
3. Run ROS system. Just run `./run.sh` inside the container.
4. Run the sim and select the highway map.
